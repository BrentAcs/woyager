class Point3d {
   constructor( x, y, z ) {
      switch ( arguments.length ) {
         case 0:
            this.x = this.y = this.z = 0;
            break;

         case 1:
            this.x = this.y = this.z = x;
            break;

         case 3:
            this.x = x;
            this.y = y;
            this.z = z;
            break;

         default:
            throw new Error("Invalid number of arguments.");
      }
   }
}

/**
 * @return {number}
 */
Point3d.prototype.DistanceTo = function( rhs ){
   var a = this.x - rhs.x;
   var b = this.y - rhs.y;
   var c = this.z - rhs.z;

   return Math.sqrt( (a * a) + (b * b) + (c * c) );
};

Point3d.prototype.Test = function(){
   console.log( "Point3d#Test(" + this.x + ")" );
};

Point3d.prototype.Test2 = function () {
   console.log( "Point3d#Test2(" + this.y + ")" );
};
