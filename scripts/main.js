function initApp() {
   var pt0 = new Point3d();
   var pt1 = new Point3d( 0, 2, 4 );
   var pt2 = new Point3d( 1, 3, 5 );
   console.log( pt1 );
   console.log( pt1.x );

   LocalStore.Clear();
   LocalStore.Set( "pt1", pt1 );
   LocalStore.Set( "pt2", pt2 );

   console.log( "attempting to get from local storage..." );

   var pt1a = LocalStore.Get( "pt1" );
   console.log( pt1a );
   console.log( pt1a.x );

   var dis = pt1.DistanceTo( pt2 );
   console.log( "distance: " + dis );

   pt1.y = 20;
   dis = pt1.DistanceTo( pt2 );
   console.log( "distance: " + dis );

   //pt1.Test();
   //pt1.Test2();

   //LocalStore.Remove( "pt2" );
}

