function Ellipsoid( a, b, c ) {
   switch ( arguments.length ) {
      case 0:
         this.a = this.b = this.c = 0;
         break;

      case 1:
         this.a = this.b = this.c = a;
         break;

      case 3:
         this.a = a;
         this.b = b;
         this.c = c;
         break;

      default:
         throw new Error( "Invalid arguments." );
   }
}

Ellipsoid.prototype.toString = function () {
   return "[ a:{0}, b:{1}, c:{2} ]".format( this.a, this.b, this.c );
};

Ellipsoid.prototype.getVolume = function () {
   return (4.0 / 3.0) * Math.PI * this.a * this.b * this.c;
};