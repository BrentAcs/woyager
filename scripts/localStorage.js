class LocalStore {
   static Clear() {
      localStorage.clear();
   }

   static Set( key, value ) {
      localStorage.setItem( key, JSON.stringify( value ) );
   }

   static Get( key ) {
      return JSON.parse( localStorage.getItem( key ) );
   }

   static Remove( key ) {
      localStorage.removeItem( key );
   }
}
